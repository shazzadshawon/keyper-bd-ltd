@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
             <div class="panel panel-success">
                <div class="panel-heading"><h4>Service Title : {{ $item->service_title }}</h4>
                    <div class="pull-right">
                        <a class="btn btn-primary" href="{{ url('edititem2/'.$item->id) }}"><span class="fa fa-upload"></span>Edit</a>
                    </div>
                </div>
                <div class="panel-body">

                   
                    <div class="block">                      
                        Description : @php
                            print_r($item->service_description );
                        @endphp
                    </div>
                </div>
             </div>
        </div>
    </div>

    <div class="row">
            <div class="col-md-10 col-md-offset-1">
                 <div class="panel panel-success">
                    

                     <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Images</h2>
                        </div>  
                        <div class="pull-right">
                         <form  class="form-inline" action="{{ url('storeitem2image/'.$item->id) }}" method="post" enctype="multipart/form-data">
                         {{ csrf_field() }}
                            <div class="form-group">
                                <input type="file" name="images[]" multiple>
                                
                            </div>
                            <button type="submit" class="btn btn-primary">Upload Images</button>
                        </form>
                    </div>                                    
                                             
                    </div>
                    
                    
                   
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="">
                        
                       
                        <div class="gallery" id="links">
                       

                             {{-- <div class="row"> --}}
                                 @foreach ($item_images as $image)
                                 {{-- <div class="col-md-3"> --}}
                                    <div class="gallery-item"  title="" data-gallery>
                                            <div class="image">                              
                                                <img style="height: 200px" src="{{ asset('public/uploads/item2/'.$image->image_name) }}" alt="{{ $image->image_name }}"/>                                        
                                                                                                              
                                            </div>
                                            <div class="row" align="">
                                               <a class="btn btn-success btn-xs col-md-6" tooltip="test" target="_blank" href="{{ asset('public/uploads/item2/'.$image->image_name) }}" ><span class="fa fa-eye"></span></a>
                                   
                                    <a class="btn btn-danger btn-xs  col-md-6" href="{{ url('deleteitem2image/'.$image->id) }}"><span class="fa fa-trash-o"></span></a>
                                            </div>
                                                                      
                                    </div>
                                    
                                     {{-- </div> --}}
                                 @endforeach
                             {{-- </div> --}}
                           

                             
                        </div>
                             
                       {{--  <ul class="pagination pagination-sm pull-right push-down-20 push-up-20">
                            <li class="disabled"><a href="#">«</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>                                    
                            <li><a href="#">»</a></li>
                        </ul> --}}
                    </div>       
                    <!-- END CONTENT FRAME BODY -->
                </div>
                               
                </div>
            </div>
        </div>
@endsection