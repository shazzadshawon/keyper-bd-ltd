<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Home | KeyPer</title>
	<meta name="description" content="KeyPer">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
	<!--[if lt IE 9]>
	  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C400italic%2C300%2C700%7CRaleway%3A400%2C200%2C600%2C700&amp;ver=4.2.6" type="text/css" media="all">
	<link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/icons/icons.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/core-icons/core_style.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/scripts.css" type="text/css" media="all">
<!--    <link rel="stylesheet" href="css/pace-theme-center-simple.css" type="text/css" media="all">-->
    <link rel="stylesheet" href="css/pace-theme-loading-bar.css" type="text/css" media="all">

	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
</head>

<body class="page">
    <!--HEADER-->
    <?php include('header.php'); ?>
    <!--///HEADER-->

    <!--SLIDER-->
    <?php include('slider.php'); ?>
    <!--///SLIDER-->

    <!--DESCRIPTION SECTION-->
    <?php include('description_section.php'); ?>
    <!--///DESCRIPTION SECTION-->


    <!--RELATED SERVICES-->
    <?php include('related_services.php');?>
    <!--///RELATED SERVICES-->

    <!-- VIDEO SECTION-->
    <?php include('video_section.php'); ?>
    <!-- ///VIDEO SECTION-->

    <!-- CLIENT REVIEW-->
	<?php include('client_review.php'); ?>
    <!-- ///CLIENT REVIEW-->

<!--	<section class="spiral_section_tc spiral-centered no_padding_top section_bg">-->
<!--		<div class="spiral_section_content">-->
<!--			<div class="spiral_container">-->
<!--				<div class="spiral_column_tc_span12">-->
<!--					<a href="#" target="_self" class="spiral-button spiral-button_light spiral-button_rounded spiral-button_medium ripplelink spiral-button_transparent ">Visit Blog</a>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</section>-->

    <!-- CONTACT FORM-->
    <?php include('contact_form.php'); ?>
    <!-- ///CONTACT FORM-->


    <!-- MAP-->
    <?php include('map.php'); ?>
    <!-- ///MAP-->

    <!--FOOTER-->
    <?php include('footer.php'); ?>
    <!--///FOOTER-->

	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="js/prettify.js"></script>
<!--	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<!--    Loader js-->
    <script type="text/javascript" data-pace-options='{ "ajax": false }' src="js/pace.min.js"></script>
<!--    ///Loader js-->
	<script type="text/javascript" src="js/portfolio-init.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>

    <script>
        jQuery( document ).ready(function() {
            localStorage.setItem("currentTab", 'home');
        });
    </script>

</body>
</html>


<!--AIzaSyCGDuZtjShvYyccGILEi6y3_9yQG1FfAfg-->