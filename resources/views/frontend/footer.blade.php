<footer id="ABdev_main_footer">
    <div id="footer_copyright">
        <div class="container">
            <div class="row">
                <div class="span4 footer_copyright left_aligned">
                    <h5><strong> <span style="color:white">Created with <i class="fa fa-heart" style="color:#a6c942" aria-hidden="true"></i> By</span> <a href="http://shobarjonnoweb.com/">Shobarjonnoweb.com</a></strong></h5>
                </div>
                <div class="span4 center_aligned">
                    <a href="#" id="back_to_top" title="Back to top">
                        <i class="ci_icon-angle-up"></i>
                    </a>
                </div>
<!--                <div class="span4 footer_credits right_aligned">-->
<!--                </div>-->
            </div>
        </div>
    </div>
</footer>