@php
    $i=0;
@endphp
@foreach ($sliders as $slide)
@if ($i%2==0)
     <li data-index="rs-38" data-transition="slideup" data-slotamount="0"  data-easein="default" data-easeout="default" data-masterspeed="500"  data-thumb="{{ asset('public/rs-plugin/assets/1bg-100x50.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Leaf Girl" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
    <!-- MAIN IMAGE -->
        <img src="{{ asset('public/uploads/slider/'.$slide->slider_image) }}"  alt=""  data-lazyload="{{ asset('public/rs-plugin/assets/s3/c2.png') }}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->
        <!-- LAYER NR. 1 -->
        <!--                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-38-layer-1" data-x="-150" data-y="-200" data-width="auto" data-height="auto" data-transform_idle="" data-transform_in="x:900;y:0;z:0;rX:20;rY:10;rZ:-50;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2500;e:Power3.easeInOut;" data-transform_out="x:900;y:0;z:0;rX:20;rY:10;rZ:-50;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;s:1000;" data-start="0" data-responsive_offset="on" style="z-index: 5;">-->
        <!--                        <img src="uploads/slider/" alt="" width="1279" height="1595" data-lazyload="rs-plugin/assets/1a.png" data-no-retina>-->
        <!--                    </div>-->
        <!-- LAYER NR. 2 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-38-layer-2" data-x="right" data-hoffset="-500" data-y="-80" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:right;s:2500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="150" data-responsive_offset="on" style="z-index: 6;">
            <img src="uploads/slider/" alt="" width="1180" height="1235" data-lazyload="rs-plugin/assets/s3/c1.png" data-no-retina>
        </div>
        <!-- LAYER NR. 3 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-3" id="slide-38-layer-3" data-x="-80" data-y="-214" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:900;y:0;z:0;rX:20;rY:10;rZ:-50;sX:1;sY:1;skX:0;skY:0;opacity:0;s:3000;e:Power3.easeInOut;" data-transform_out="x:900;y:0;z:0;rX:20;rY:10;rZ:-50;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1000;s:1000;" data-start="100" data-responsive_offset="on" style="z-index: 7;">
            <img src="uploads/slider/" alt="" width="1742" height="1436" data-lazyload="rs-plugin/assets/s3/1c.png" data-no-retina>
        </div>
        <!-- LAYER NR. 4 -->
        <div class="tp-caption Raleway   tp-resizeme" id="slide-38-layer-4" data-x="-19" data-y="437" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 8; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00);color:#5c5c5c;">
            <strong>KEYPER</strong>
            <br>
            <small>RESERVE YOUR KEYS</small>
        </div>
        <!-- LAYER NR. 5 -->
        <div class="tp-caption Raleway_small   tp-resizeme" id="slide-38-layer-5" data-x="-21" data-y="660" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1600;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="550" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-captionhidden="on" style="z-index: 9; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00); color:#a6ca43;">
            KEYPER  is  advertising,  marketing,  and  event Management <br>  Company  provides  a  wide  range of services to ensure that every service is a complete success.
        </div>
        <!-- LAYER NR. 6 -->
        <div class="tp-caption none   tp-resizeme" id="slide-38-layer-6" data-x="6" data-y="806" data-width="auto" data-height="auto" data-transform_idle="" data-transform_in="y:50px;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="1600" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 10; white-space: nowrap;border-color:rgba(34, 34, 34, 1.00);">
            <a href='about.php' class="rev_slider_button_red">About Us</a>
        </div>
        <!-- LAYER NR. 7 -->
        <div class="tp-caption none   tp-resizeme" id="slide-38-layer-7" data-x="200" data-y="806" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:50px;opacity:0;s:2000;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;" data-start="1600" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-captionhidden="on" style="z-index: 10; white-space: nowrap;border-color:#5c5c5c;">
            <a href='contact.php' class="rev_slider_button_white" style="background: #5c5c5c">Contact Us</a>
        </div>
    </li>

@else
    
    <li data-index="rs-41" data-transition="slidedown" data-slotamount="7"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=""  data-rotate="0"  data-saveperformance="on"  data-title="Devices Desk" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('public/uploads/slider/'.$slide->slider_image) }}" style='background-color:#5A5A5A' alt=""  data-lazyload="{{ asset('public/rs-plugin/assets/s2/transparent.png') }}" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-1" data-x="center" data-hoffset="0" data-y="-150" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:top;s:1250;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="500" data-responsive_offset="on" style="z-index: 5;">
                        <img src="{{ asset('public/uploads/slider/'.$slide->slider_image) }}" alt="" width="1237" height="614" data-lazyload="{{ asset('public/rs-plugin/assets/s2/3b1.jpg') }}" data-no-retina>
                    </div>
                    <!-- LAYER NR. 2 -->
                    <!-- <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-2" data-x="1086" data-y="310" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:right;s:1450;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="550" data-responsive_offset="on" style="z-index: 6;">
                        <img src="uploads/slider/" alt="" width="310" height="416" data-lazyload="rs-plugin/assets/s2/3c.jpg" data-no-retina>
                    </div> -->
                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-3" data-x="821" data-y="587" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:right;s:1400;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="500" data-responsive_offset="on" style="z-index: 7;">
                        <img src="{{ asset('public/uploads/slider/'.$slide->slider_image) }}" alt="" width="240" height="263" data-lazyload="{{ asset('public/rs-plugin/assets/s2/3d.jpg') }}" data-no-retina>
                    </div>
                    <!-- LAYER NR. 4 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-4" data-x="-300" data-y="center" data-voffset="0" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="x:left;s:1500;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="580" data-responsive_offset="on" style="z-index: 8;">
                        <img src="{{ asset('public/uploads/slider/'.$slide->slider_image) }}" alt="" width="589" height="621" data-lazyload="{{ asset('public/rs-plugin/assets/s2/3a.jpg') }}" data-no-retina>
                    </div>
                    <!-- LAYER NR. 5 -->
                    <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" id="slide-41-layer-5" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="143" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1000;e:Power4.easeInOut;" data-transform_out="auto:auto;s:300;" data-start="625" data-responsive_offset="on" style="z-index: 9;">
                        <img src="uploads/slider/" alt="sdfdf" width="231" height="62" data-lazyload="rs-plugin/assets/s2/3e.jpg" data-no-retina>
                    </div>
                    <!-- LAYER NR. 6 -->
                    <div class="tp-caption Raleway   tp-resizeme" id="slide-41-layer-6" data-x="center" data-hoffset="0" data-y="center" data-voffset="0" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:1500;e:Power3.easeInOut;" data-transform_out="y:top;s:300;s:300;" data-start="250" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-captionhidden="on" style="z-index: 10; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00);">
                        <div style="text-align:center;">PASSION<br>
                            <strong>FOR PERFECTION</strong>
                        </div>
                    </div>
                    <!-- LAYER NR. 7 -->
                    <div class="tp-caption Raleway_small   tp-resizeme" id="slide-41-layer-7" data-x="center" data-hoffset="0" data-y="center" data-voffset="180" data-width="auto" data-height="auto" data-transform_idle=""data-transform_in="y:bottom;s:2000;e:Power4.easeInOut;" data-transform_out="y:top;s:300;s:300;" data-start="250" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 11; white-space: nowrap;border-color:rgba(255, 255, 255, 1.00);">
                        <div style="text-align: center;">Else<br>

                        </div>
                    </div>
</li>

@endif
   
   @php
       $i++;
   @endphp


@endforeach
<!-- SLIDE  -->


<!-- SLIDE  -->