<div class="banner-bottom">
    <!--//screen-gallery-->
    <div class="wthree_title_agile">
        <h3>Our <span>Team</span></h3>
        <!-- <p><i class="fa fa-heart-o" aria-hidden="true"></i></p> -->
    </div>
    <!-- <p class="sub_para">WE ARE GETTING MARRIED</p> -->
    <div class="inner_w3l_agile_grids">
        <div class="sreen-gallery-cursual">
            <div id="team_slider" class="owl-carousel">
            @php
                $teams = DB::table('teams')->get();
            @endphp
            @foreach ($teams as $team)
                <div class="item-owl">
                    <div class="test-review">
                        <img style="height: 150px" src="{{ asset('public/uploads/team/'.$team->team_image) }}" class="img-responsive" alt=""/>
                        <h5>{{$team->team_title}}</h5>
                        <h3>{{$team->team_description}}</h3>
                        <h3>{{$team->team_contact}}</h3>
                    </div>
                </div>
            @endforeach
               
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#team_slider").owlCarousel({
            items :5,
            itemsDesktop : [768,4],
            itemsDesktopSmall : [414,3],
            lazyLoad : true,
            autoPlay : true,
            navigation :true,
            navigationText :  false,
            pagination : true,
        });
    });
</script>