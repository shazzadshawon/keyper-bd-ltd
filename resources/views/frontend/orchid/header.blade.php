<header>
    <div class="container">
        <!-- nav -->
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo">
                        <h1><a href="{{ url('/') }}"><span></span> <div class="logo-img"><img src="{{ asset('public/images/logo_made.png') }}" alt="Logo" /></div><p class="sub_title">Orchid Garden</p> </a></h1>
                    </div>
                    
                </div>
@php
    $weds = DB::table('services')->get();
    $intcats = DB::table('int_categories')->get();
@endphp
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <nav class="cl-effect-13" id="cl-effect-13">
                        <ul class="nav navbar-nav">
                            <li class="menu-item active"><a href="index.php">Home</a></li>
                            <li class="menu-item dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Wedding<b class="caret"></b></a>
                                <ul class="dropdown-menu agile_short_dropdown">
                                @foreach ($weds as $wed)
                                    <li><a href="{{ asset('wedding_event/'.$wed->id) }}">{{ $wed->service_title }}</a></li>
                                @endforeach
                               

{{-- @if (!empty())
@endif --}}

                                </ul>
                            </li>
                            <li class="menu-item dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Interior &amp; Exterior <b class="caret"></b></a>
                                <ul class="dropdown-menu agile_short_dropdown">
                                @foreach ($intcats as $incat)
                                    <li class="menu-item dropdown-submenu">
                                    @php
                                        $ints = DB::table('interior')->where('service_sub_cat_id',$incat->id)->get();
                                    @endphp
                                        <a class="dropdown-toggle" data-toggle="" href="{{ url('allint/'.$incat->id) }}">{{ $incat->cat_name }}</a>
                                        @if ($ints)
                                        <ul class="dropdown-menu">
                                            @foreach ($ints as $int)
                                                <li><a href="{{ asset('int/'.$int->id) }}">{{ $int->service_title }}</a></li>
                                            @endforeach
                                        </ul>
                                        @endif
                                        
                                    </li>
                                @endforeach
                                   
                                </ul>
                            </li>
                            

                            <li><a href="{{ url('gallery') }}">Gallery</a></li>
                            <!-- <li><a href="contact.html">Contact Us</a></li> -->
                            <li class="menu-item dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Contact<b class="caret"></b></a>
                                <ul class="dropdown-menu agile_short_dropdown">
                                    <li><a href='{{ url('client_contact') }} '>Client's Choice</a></li>
                                    <li><a href='{{ url('og_contact') }} '>OG's choice</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <!-- //nav -->
    </div>
</header>