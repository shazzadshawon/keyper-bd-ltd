<div class="agileinfo_counter_section">
{{--  --}}
    <div class="wthree_title_agile">
        <h3 class="h-t">Event <span>Coming Soon</span></h3>
        <p><i class="fa fa-bomb" aria-hidden="true"></i></p>

    </div>
    <p class="sub_para two event_soon">{{ $event->event_name }}</p>
    <div class="wthree-counter-agile">
        <section class="examples">
            <div class="simply-countdown-losange" id="simply-countdown-losange"><span id="tracker" hidden="true">{{ $event->event_date }}</span></div>
            <div class="clearfix"></div>
        </section>
    </div>
    <div class="clearfix"></div>
</div>

<script src="{{ asset('public/js/frontend/simplyCountdown.js') }}"></script>
<script type="text/javascript">
    
$(".agileinfo_counter_section").css("background-image","url("+ {{ asset('public/images/event_soon/event_soon.jpg') }} +")");
</script>
<script>
    var eventTime = $('#tracker').html();
    console.log(eventTime);
    var d = new Date(eventTime);    
    // var d = new Date(new Date().getTime() + 948 * 120 * 120 * 2000);    

    // default example
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate()
    });

    // inline example
    simplyCountdown('.simply-countdown-inline', {
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        inline: true
    });

    //jQuery example
    $('#simply-countdown-losange').simplyCountdown({
        year: d.getFullYear(),
        month: d.getMonth() + 1,
        day: d.getDate(),
        enableUtc: false
    });
</script>