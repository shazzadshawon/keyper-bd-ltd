<!DOCTYPE html>
<html lang="zxx">
<head>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"><!-- fontawesome css -->
   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"><!-- Bootstrap stylesheet -->
    <link href="{{ asset('public/css/frontend/jquery.fancybox.css') }}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{ asset('public/css/frontend/snow.css') }}" rel="stylesheet" type="text/css" media="all" /><!-- stylesheet -->
    <link href="{{ asset('public/css/frontend/lsb.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('public/css/frontend/style.css') }}" rel="stylesheet" type="text/css" media="all" /><!-- stylesheet -->
    <!-- meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Wedding Proposer Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
    Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //meta tags -->
    <!--fonts-->
    <link href="//fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700" rel="stylesheet">
    <!--//fonts-->
</head>
<body>
    <!-- header -->
    @include('frontend.header') 
	<!-- //header -->
	<!-- banner-slider -->
	   <div class="">
      <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   </div>
	<!-- banner-slider -->
	<!-- breadcrumbs -->
	<div class="w3l_agileits_breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="{{ url('/') }}">Home</a><span>«</span></li>
				<li>Gallery</li>
			</ul>
		</div>
	</div>
	<!-- //breadcrumbs -->

	<!--/story-->
	<div class="w3l_inner_section gallery">
		<div class="container-fluid">
		    <div class="wthree_title_agile">
		        <h2>Gallery</h2>
				<p><i class="fa fa-television" aria-hidden="true"></i></p>
			</div>
			<img class="img-responsive loader_image" src="{{ asset('public/images/loading.gif') }}" style="margin: 0 auto">
            <section class="gallery-part" id="gallary">
                <div class="gallery_loader" style="display: none">
                    <div class="tabbing-wrapper">
                        <button title="Tooltip on left" data-placement="left" data-toggle="tooltip" class="btn btn-default" data-filter="all" type="button">
                            ALL
                        </button>
                        <button title="Tooltip on top" data-placement="top" data-toggle="tooltip" class="btn btn-default" data-filter="wedding" type="button">
                            WEDDING
                        </button>
                        <button title="Tooltip on bottom" data-placement="bottom" data-toggle="tooltip" class="btn btn-default" data-filter="music" type="button">
                            MUSIC
                        </button>
                        <button title="Tooltip on right" data-placement="right" data-toggle="tooltip" class="btn btn-default" data-filter="event" type="button">
                            EVENT
                        </button>
                    </div>
                </div>
                <div class="gallery-blog">
                    <ul class="gallery-img-sec clearfix"></ul>
                </div>
            </section>
		</div>
	</div>
	<!--//story-->
	<!-- footer -->
     @include('frontend.footer') 

	<script type="text/javascript" src="{{ asset('public/js/frontend/jquery-2.1.4.min.js') }}"></script><!-- Required-js -->
    <script src="{{ asset('public/js/frontend/isotope.pkgd.min.js') }}"></script> <!-- Filering -->
    <script src="{{ asset('public/js/frontend/packery-mode.pkgd.min.js') }}"></script>
    <script src="{{ asset('public/js/frontend/jquery.fancybox.js') }}"></script>
	<script src="{{ asset('public/js/frontend/responsiveslides.min.js') }}"></script>

    <script>
        // Filltering
        $(document).ready(function(){
            //EVENT
            var event_folder = "public/images/gallery/events/";
            $.ajax({
                url : event_folder,
                success: function (event_data) {
                    $(event_data).find("a").attr("href", function (i, val) {
                        if( val.match(/\.(jpe?g|png)$/) ) {
//                            $("body").append( "<img src='"+ folder + val +"'>" );
                            var src = event_folder + val;
                            var event = '<li class="main-item grid-sizer all event">'+
                                '<div class="grid">' +
                                '<a href="'+src+'" class="lsb-preview" data-lsb-group="header">' +
                                '<figure class="effect-winston">' +
                                '<img src="'+src+'" class="img-responsive" alt=" " />' +
                                '<figcaption>' +
                                '<p>' +
                                '<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>' +
                                '<span class="glyphicon glyphicon-heart" aria-hidden="true"></span>' +
                                '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>' +
                                '</p>' +
                                '</figcaption>' +
                                '</figure>' +
                                '</a>' +
                                '</div>' +
                                '</li>';
                            $('.gallery-img-sec').append(event);
                        }
                    });
                    loadMusic();
                }
            });
        });

        function loadMusic(){
            var folder = "public/images/gallery/music/";
            $.ajax({
                url : folder,
                success: function (music_data) {
                    $(music_data).find("a").attr("href", function (i, val) {
                        if( val.match(/\.(jpe?g|png)$/) ) {
                            var src = folder + val;
                            var music = '<li class="main-item grid-sizer all music">'+
                                '<div class="grid">' +
                                '<a href="'+src+'" class="lsb-preview" data-lsb-group="header">' +
                                '<figure class="effect-winston">' +
                                '<img src="'+src+'" class="img-responsive" alt=" " />' +
                                '<figcaption>' +
                                '<p>' +
                                '<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>' +
                                '<span class="glyphicon glyphicon-heart" aria-hidden="true"></span>' +
                                '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>' +
                                '</p>' +
                                '</figcaption>' +
                                '</figure>' +
                                '</a>' +
                                '</div>' +
                                '</li>';
                            $('.gallery-img-sec').append(music);
                        }
                    });
                    loadWedding();
                }
            });
        }

        function loadWedding(){
            var wedding_folder = "public/images/gallery/wedding/";
            $.ajax({
                url : wedding_folder,
                success: function (wedding_data) {
                    $(wedding_data).find("a").attr("href", function (i, val) {
                        if( val.match(/\.(jpe?g|png)$/) ) {
                            var src = wedding_folder + val;
                            var wedding = '<li class="main-item grid-sizer all wedding">'+
                                '<div class="grid">' +
                                '<a href="'+src+'" class="lsb-preview" data-lsb-group="header">' +
                                '<figure class="effect-winston">' +
                                '<img src="'+src+'" class="img-responsive" alt=" " />' +
                                '<figcaption>' +
                                '<p>' +
                                '<span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>' +
                                '<span class="glyphicon glyphicon-heart" aria-hidden="true"></span>' +
                                '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>' +
                                '</p>' +
                                '</figcaption>' +
                                '</figure>' +
                                '</a>' +
                                '</div>' +
                                '</li>';
                            $('.gallery-img-sec').append(wedding);
                        }
                    });
                }
            });
        }

        $(window).load(function(){
            if ($('.gallery-part .gallery-img-sec').length) {

                var $container = $('.gallery-img-sec').isotope({
                    layoutMode : 'packery',
                    itemSelector : '.main-item',
                    percentPosition: true,
                    packery: {
//                        gutter: 10
                        columnWidth: '.grid-sizer'
                    } //grid-sizer
                })

                $('.gallery-part .tabbing-wrapper button').on('click', function() {

                    var filterValue = "." + $(this).attr('data-filter');
                    $container.isotope({
                        filter : filterValue
                    });
                    var fancybox = $(this).attr('data-filter');
                    $(filterValue).find('a').attr({
                        'data-fancybox-group' : fancybox
                    });

                });
            }
        });

        if ($('.fancybox-button').length) {
            $(".fancybox-button").fancybox({
                prevEffect : 'none',
                nextEffect : 'none',
                closeBtn : true,
                helpers : {
                    title : {
                        type : 'inside'
                    },
                    buttons : {}
                }
            });
        }

        $('.control').on('click', function() {
            $(this).remove();
            var video = '<iframe src="' + $('.video img').attr('data-video') + '"></iframe>'
            $('.video img').after(video);
            return false;
        });

        if ($('.project').length) {
            $('.project').appear(function() {
                $('.project').countTo({
                    from : 0,
                    to : 178
                });

                $('.award').countTo({
                    from : 0,
                    to : 16
                });

                $('.montre').countTo({
                    from : 0,
                    to : 178
                });

                $('.hours').countTo({
                    from : 0,
                    to : 2600
                });

            });

        }
        // You can also use "$(window).load(function() {"
        $(function () {
          // Slideshow 4
          $("#slider3").responsiveSlides({
            auto: true,
            pager:true,
            nav:false,
            speed: 500,
            namespace: "callbacks",
            before: function () {
              $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
              $('.events').append("<li>after event fired.</li>");
            }
          });

        });
     </script>
     <!-- js -->

    <!-- //main slider-banner -->

    <script src="{{ asset('public/js/frontend/lsb.min.js') }}"></script>


    <script>
    $(window).load(function() {
        $('.gallery_loader').show();
        $('.loader_image').hide();
        $.fn.lightspeedBox();
    });
    </script>
    <!-- //js -->

    <!-- here stars scrolling icon -->
    <script type="text/javascript">
        $(document).ready(function() {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear'
                };
            */

            $().UItoTop({ easingType: 'easeOutQuart' });

            });
    </script>
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="{{ asset('public/js/frontend/move-top.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/easing.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->
    <!-- //here ends scrolling icon -->
    <script src="{{ asset('public/js/frontend/bootstrap.min.js') }}"></script><!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
</body>
</html>
