<section id="description" class="spiral_section_tc section_with_header">
    <header style="background: rgba(166, 201, 66, 0.24) !important;">
        <div class="spiral_container">
            <h3>Welcome to Have a Glimpse of our
                <span class="spiral-animo description_box" data-animation="bounceInUp" data-trigger_pt="0" data-duration="2000" data-delay="0"><a href="{{ url('/#related') }}"><strong>WORK</strong></a></span>, your interest will drag you to know more
                <span class="spiral-animo description_box" data-animation="bounceInUp" data-trigger_pt="0" data-duration="2000" data-delay="0"><a href="{{ url('/about') }}"><strong>ABOUT</strong></a></span>
                us and then you will feel interest to
                <span class="spiral-animo description_box" data-animation="bounceInUp" data-trigger_pt="0" data-duration="2000" data-delay="0"><a href="{{ url('contact') }}"><strong>CONTACT</strong></a></span> us.</h3>
        </div>
    </header>
</section>