<section class="spiral_section_tc section_body_fullwidth no_padding">
    <div class="spiral_section_content">
        <div class="spiral_container">
            <div class="spiral_column_tc_span12">
                <div class="spiral_google_map_wrapper">
                    <div id="spiral_google_map_1" data-map_type="ROADMAP" data-auto_center_zoom="0" data-lat="23.76164" data-lng="90.3659569" data-zoom="16" data-scrollwheel="0" data-maptypecontrol="1" data-pancontrol="1" data-zoomcontrol="1" data-scalecontrol="1" class="spiral_google_map google_map_style_2">
                    </div>
                    <div class="spiral_google_map_marker" data-title="Keyper LTD" data-icon="{{ asset('public/uploads/pin.png') }}" data-lat="23.761895" data-lng="90.368189">
                        <h5>11/15 Iqbal Road, Mohammadpur, Dhaka - 1207</h5>
                        <p class="no_margin_bottom">11/15 Iqbal Road, Mohammadpur, Dhaka - 1207</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5WY3dr0I-QsSbHaCqf9Z1-fYI2nWYmms"></script>


<!-- old key ==> AIzaSyCGDuZtjShvYyccGILEi6y3_9yQG1FfAfg -->
<!-- API key for admin@keyperltd.com ==> AIzaSyC5WY3dr0I-QsSbHaCqf9Z1-fYI2nWYmms -->