<section id="related" class="spiral_section_tc spiral-inversed_text section_body_fullwidth section_with_header section_bg">
    <header>
        <div class="spiral_container">
            <h3>Related
                <strong>Services</strong>
            </h3>
        </div>
    </header>
    <div class="spiral_section_content">
        <div class="spiral_container">
            <div class="spiral_column_tc_span12 center_aligned spiral-animo" data-animation="bounceInUp" data-trigger_pt="0" data-duration=" " data-delay="0">
                <div class="spiral_featured_portfolio clearfix">

                    <!-- one thumbline view is linked for example from related_services_thumbline_view page-->
                   {{--  @include('frontend.related_services_thumbline_view') --}}
                    <!--///thumbline view-->

                    @foreach ($works as $work)
                    @php
                        $image = DB::table('item_images_1')
                        ->where('item_id',$work->id)
                        ->first();
                    @endphp
                         <div class="portfolio_item featured_item  illustrations" data-post_id="84" data-name="" data-thumbnail='&lt;img src="{{ asset('public/uploads/item1/'.$image->image_name) }}" alt="portfolio2" /&gt;' data-image_link="{{ asset('public/uploads/item1/'.$image->image_name) }}" data-link_to="{{ url('work/'.$work->id) }}" data-cat="" data-date="" data-skills="" data-client="" data-description="" data-number="2">
                            <div class="overlayed">
                                 <a class="" href="{{ url('work/'.$work->id) }}">
                                <img style="" class="related_services_poster" src="{{ asset('public/uploads/item1/'.$image->image_name) }}" alt="portfolio8">
                                </a>
                                <div class="overlay">
                                    <div class="portfolio_icons_container">
                                        <a class="portfolio_icon" href="{{ url('work/'.$work->id) }}">
                                            <i class="ci_icon-new-window"></i>
                                        </a>
                                    </div>
                                    <p class="overlay_title">{{ $work->service_title }}</p>
                                    <p class="portfolio_item_tags">
                                       {{--  @php
                                            print_r($work->service_description);
                                        @endphp --}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                   
                  


                </div>
<!--                SLIDER DETAILS PAGE-->
                {{-- @include('frontend.related_services_detailsSlide') --}}
<!--                SLIDER DETAILS PAGE-->

                <span class="clear spacer_29"></span>
               {{--  <a href="{{ url('/#contact') }}" target="_self" class="spiral-button spiral-button_light spiral-button_rounded spiral-button_medium ripplelink spiral-button_transparent ">Contact Us</a> --}}
            </div>
        </div>
    </div>
</section>