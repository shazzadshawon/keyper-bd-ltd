@extends('layouts.frontend')

@section('content')

	<section id="headline_breadcrumbs_bar" class="with_image">
		<div class="headline_image headline_style_about_us">
			<div class="container">
				<div class="row">
					<div class="span12 left_aligned headline_title">
						<h2>About us</h2>
					</div>
					<h3 class="white_text page_meta_description">
						This is a short description of this page.
					</h3>
					<div id="button_social" class="headline_button">
						<span class="social_button_title">Share this Page</span>
						<span class="social_button_contents">
							<a class="spiral_tooltip" data-gravity="s" href="http://www.facebook.com/keyperltd/" target="_blank" title="Follow us on Facebook"><i class="tmf-facebook"></i></a>
						    <a class="spiral_tooltip" data-gravity="s" href="http://www.instagram.com/keyperltd/" target="_blank" title="Follow us on Instagram"><i class="tmf-instagram"></i></a>
						    <a class="spiral_tooltip" data-gravity="s" href="http://bd.linkedin.com/in/keyper-bd-23a21770" target="_blank" title="Follow us on Linkedin"><i class="tmf-linkedin"></i></a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="who_we_are" class="spiral_section_tc section_with_header no_padding_bottom">
		<header>
			<div class="spiral_container">
				<h3>
					Who <strong>We</strong> Are
				</h3>
			</div>
		</header>
		<div class="spiral_section_content green_background">
			<div class="spiral_container">
				<div class="spiral_column_tc_span6">
					<h2 class="gray_text no_margin_top">
						<span>
							KEYPER is advertising, marketing, <br>
							and event Management Company <br>
							provides a wide range of services <br>
							to ensure that every advertising, <br>
							marketing, and event project are <br>
							everything connects –<br>
							a complete success.
						</span>
					</h2>
					<span class="clear spacer_25"></span>
					<div>
						<p>KEYPER can assist you with the details that can often become a hassle</p>
					</div>
					<div>
						<p>Areas such as contract
							administration and production-based issues, such as sourcing and coordinating suppliers and
							vendors, including day-of-the-event management, can be organized and taken care of by
							KEYPER enabling you the luxury of more time and focused concentration on other areas of your
							event.</p>
					</div>
				</div>
				<div class="spiral_column_tc_span6">
					<div class="spiral-accordion " data-expanded="1">
						<h3>
							Vision of Keyper
						</h3>
						<div class="spiral-accordion-body">
							<p>Whatever your creative needs... we deliver!</p>
							<p>KEYPER has aimed to be the top event organizer and manager in Bangladesh within 5 years.</p>
							<ul class="list-group">
								<li class="list-group-item">to work with our clients for their long term benefit</li>
								<li class="list-group-item">to constantly research and develop new strategies, technologies and skills</li>
								<li class="list-group-item">to provide exceptional service</li>
								<li class="list-group-item">to maintain constant communication</li>
							</ul>
						</div>
						<h3>
							Our Mission
						</h3>
						<div class="spiral-accordion-body">
							We shall continuously fight to reach our objective. The
							combination of commitment, determination and focus
							will do that we obtain the objectives of our customers and thereby our own.
						</div>
						<h3>
							Our Values
						</h3>
						<div class="spiral-accordion-body">
							<ul class="list-group">
								<li class="list-group-item">Responsibility</li>
								<li class="list-group-item">Trust</li>
								<li class="list-group-item">Confidence</li>
								<li class="list-group-item">Creativity &amp; Innovation</li>
							</ul>
						</div>
						<h3>
							Our Expertise
						</h3>
						<div class="spiral-accordion-body">
							<ul class="list-group">
								<li class="list-group-item">Design &amp; Engineering</li>
								<li class="list-group-item">Maintenance</li>
								<li class="list-group-item">Management</li>
								<li class="list-group-item">Proposal Preparation</li>
								<li class="list-group-item">Sourcing</li>
								<li class="list-group-item">Development</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>



	<section id="why_the_name" class="spiral_section_tc section_with_header no_padding_bottom">
		<header>
			<div class="spiral_container">
				<h3>
					Why The Name <strong>KEYPER</strong>
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12">
					<h2 class="gray_text no_margin_top">
						<span>
							This is because we use the object <q>key</q> everyday without much caring
							about the value of this, up until we lose this priceless piece of metal.
							We at keyper are there to help you even when you have lost your key.
							At Keyper we reserve all the keys according to your needs and requirements.
						</span>
					</h2>
				</div>
			</div>
		</div>
	</section>



	<section id="our_voice" class="spiral_section_tc section_with_header no_padding_bottom">
		<header>
			<div class="spiral_container">
				<h3>
					Our <strong>Voice</strong>
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12">
					<div>
						<p>KEYPER can provide full advertising, marketing & event management and planning,
							including the creation and development of thematic concepts, food and beverage services,
							decoration and décor, and full entertainment for any event or campaign regardless of size or budget
						</p>
					</div>
					<div>
						<p>KEYPER can implement sponsorship initiatives and marketing strategies for your projects and events.
							There are numerous avenues to explore, and a multitude of resources available; KEYPER is
							fundamental to ensure that your project and event shines above all the rest.
						</p>
					</div>
					<div>
						<p>Developed by a pool of talented and experienced people, KEYPER has aimed to change ordinary
							events into exclusive and exceptional ones through creative ideas and outstanding innovations.
							From idea generation to the completion of a program, we carry out every single task that adds
							value to the events.
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	
	<section class="spiral_section_tc column_title_left section_with_header no_margin_top green_background">
		<header>
			<div class="spiral_container">
				<h3>
					Our <strong>Services</strong>
				</h3>
			</div>
		</header>
		<div class="spiral_section_content">
			<div class="spiral_container">
				<div class="spiral_column_tc_span12">
					<div>
						<p>
							KEYPER formulates corporate events such as product launching, annual meetings, etc in a more
							objective-oriented way depending on the organization’s need. We possess exceptionality in
							designing these events and try to make the best of our efforts. Through our services, you will
							find yourselves one step ahead from your competitors. Following are our services in the
							corporate field:
						</p>
					</div>
				</div>
				<div class="spiral_column_tc_span1 responsive_hide"></div>
			</div>
			<div class="spiral_container">
				<div class="spiral_column_tc_span3">

					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Art &amp; Precision
							</h3>
						</div>
						<p>Art &amp; Precision</p>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Corporate Meetings
							</h3>
						</div>
						<p>Annual General ,Board Meetings</p>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Press conferences &amp; <br> meetings
							</h3>
						</div>
						<p>Press conferences &amp; Meetings</p>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Award-giving ceremonies
							</h3>
						</div>
						<p>Award-giving ceremonies</p>
					</div>
				</div>
			</div>
			<div class="spiral_container">
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Road Show
							</h3>
						</div>
						<p>Road Show</p>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Gift Item Supply
							</h3>
						</div>
						<p>Gift Item Supply</p>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Corporate picnic
							</h3>
						</div>
						<p>Corporate picnics</p>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Conferences &amp; Seminars
							</h3>
						</div>
						<p>Conferences &amp; Seminars</p>
					</div>
				</div>
			</div>
			<div class="spiral_container">
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Corporate conventions
							</h3>
						</div>
						<p>Corporate conventions</p>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Corporate sports events
							</h3>
						</div>
						<p>Corporate sports events </p>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								Corporate workshops
							</h3>
						</div>
						<p>Corporate workshops  </p>
					</div>
				</div>
				<div class="spiral_column_tc_span3">
					<div class="spiral_metro_box metro_box_bg1">
						<div class="spiral_metro_box_header">
							<h3 class="auto_width">
								CSR activities
							</h3>
						</div>
						<p>CSR activities  </p>
					</div>
				</div>

			</div>
		</div>
	</section>

@endsection

