<section class="spiral_section_tc spiral-centered spiral-inversed_text section_body_fullwidth section_with_header section_bg no_padding_bottom">
    <header>
        <div class="spiral_container">
            <h3>Client
                <strong>Reviews</strong>
            </h3>
        </div>
    </header>
    <div class="spiral_section_content">
        <div class="spiral_container">
            <div class="spiral_column_tc_span12 spiral-animo" data-animation="bounceInRight" data-trigger_pt="0" data-duration="1000" data-delay="0">
                <div class="spiral_post_excerpt_carousel" data-duration="800">
                    <div class="carousel_navigation">
                        <a href="#" class="carousel_prev">
                            <i class="ci_icon-angle-left"></i>
                        </a>
                        <a href="#" class="carousel_next">
                            <i class="ci_icon-angle-right"></i>
                        </a>
                    </div>
                    <ul class="clearfix">
                        <li class="first">
                            @include('frontend.client_review_item')
                           
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>