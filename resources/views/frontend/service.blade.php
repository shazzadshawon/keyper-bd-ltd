@extends('layouts.frontend')

@section('content')


	<section id="headline_breadcrumbs_bar" class="hadline_no_image shop">
		<div class="container">
			<div class="row">
				<div class="span8 left_aligned headline_title">
					<h2>
						{{ $service->service_title }}
					</h2>
				</div>
				<div class="span4 right_aligned">
					<span class="social_share_shop white_text">
<!--						<span class="text">Share this Page:</span> -->
                        <a class="spiral_tooltip" data-gravity="s" href="http://www.facebook.com/keyperltd/" target="_blank" title="Follow us on Facebook"><i class="tmf-facebook"></i></a>
						<a class="spiral_tooltip" data-gravity="s" href="http://www.instagram.com/keyperltd/" target="_blank" title="Follow us on Instagram"><i class="tmf-instagram"></i></a>
						<a class="spiral_tooltip" data-gravity="s" href="http://bd.linkedin.com/in/keyper-bd-23a21770" target="_blank" title="Follow us on Linkedin"><i class="tmf-linkedin"></i></a>
					</span>
				</div>
			</div>
		</div>
	</section>
	<section id="simple_item_portfolio">
		<div class="container">
			<div class="row">
				<div class="span8">
					<div class="slider-wrapper theme-default">
						<div id="portfolio_gallery_slider" class="nivoSlider">
						@foreach ($images as $image)
							<img style="height: 400px;" src="{{ asset('public/uploads/item2/'.$image->image_name) }}" data-thumb="{{ asset('public/uploads/item2/'.$image->image_name) }}" alt="">
						@endforeach
							
						</div>
					</div>
				</div>
				<div class="span4 portfolio_item_meta">
					<h2 class="project_description_title">
						Project Description
					</h2>
					<p class="portfolio_single_description">
                       @php
                       	print_r($service->service_description);
                       @endphp
					</p>
					{{-- <p class="portfolio_item_view_link">
						<a href="#" target="_blank" class="spiral-button spiral-button_red spiral-button_rounded spiral-button_large ripplelink spiral-button_transparent">View project</a>
					</p> --}}
				</div>
			</div>
		</div>
	</section>



	@endsection