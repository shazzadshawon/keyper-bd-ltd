{{-- <!DOCTYPE html>
<html lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Details | KeyPer</title>
	<meta name="description" content="KeyPer">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
	<!--[if lt IE 9]>
	  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C400italic%2C300%2C700%7CRaleway%3A400%2C200%2C600%2C700&amp;ver=4.2.6" type="text/css" media="all">
	<link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/icons/icons.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/core-icons/core_style.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/scripts.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
</head>
 --}}

 @extends('layouts.frontend')

@section('content')

<body class="page">
    <!--HEADER-->
    <?php include('header.php'); ?>
    <!--///HEADER-->

	<section id="headline_breadcrumbs_bar" class="hadline_no_image shop">
		<div class="container">
			<div class="row">
				<div class="span8 left_aligned headline_title">
					<h2>
						Sub Category Name – Details
					</h2>
				</div>
				<div class="span4 right_aligned">
					<span class="social_share_shop white_text">
<!--						<span class="text">Share this Page:</span> -->
                        <a class="spiral_tooltip" data-gravity="s" href="http://www.facebook.com/keyperltd/" target="_blank" title="Follow us on Facebook"><i class="tmf-facebook"></i></a>
						<a class="spiral_tooltip" data-gravity="s" href="http://www.instagram.com/keyperltd/" target="_blank" title="Follow us on Instagram"><i class="tmf-instagram"></i></a>
						<a class="spiral_tooltip" data-gravity="s" href="http://bd.linkedin.com/in/keyper-bd-23a21770" target="_blank" title="Follow us on Linkedin"><i class="tmf-linkedin"></i></a>
					</span>
				</div>
			</div>
		</div>
	</section>
	<section id="simple_item_portfolio">
		<div class="container">
			<div class="row">
				<div class="span8">
					<div class="slider-wrapper theme-default">
						<div id="portfolio_gallery_slider" class="nivoSlider">
							<img src="images/details/demo.png" data-thumb="images/details/demo.png" alt="">
							<img src="images/details/demo1.jpg" data-thumb="images/details/demo1.jpg" alt="">
							<img src="images/details/demo2.jpg" data-thumb="images/details/demo2.jpg" alt="">
							<img src="images/details/demo3.jpg" data-thumb="images/details/demo3.jpg" alt="">
							<img src="images/details/demo4.jpg" data-thumb="images/details/demo4.jpg" alt="">
							<img src="images/details/demo5.jpg" data-thumb="images/details/demo5.jpg" alt="">
							<img src="images/details/demo6.jpg" data-thumb="images/details/demo6.jpg" alt="">
						</div>
					</div>
				</div>
				<div class="span4 portfolio_item_meta">
					<h2 class="project_description_title">
						Project Description
					</h2>
					<p class="portfolio_single_description">
                        is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</p>
					<p class="portfolio_single_detail">
						<span class="portfolio_item_meta_label">Date:</span>
						<span class="portfolio_item_meta_data">July 25, 2017</span>
					</p>
					<p class="portfolio_single_detail">
						<span class="portfolio_item_meta_label">Client:</span>
						<span class="portfolio_item_meta_data">Company Name</span>
					</p>
					<p class="portfolio_single_detail">
						<span class="portfolio_item_meta_label">Skills:</span>
						<span class="portfolio_item_meta_data">Drawing, Illustrator, Photoshop</span>
					</p>
					<p class="portfolio_single_detail">
						<span class="portfolio_item_meta_label">Category:</span>
						<span class="portfolio_item_meta_data">
							Design, Illustrations
						</span>
					</p>
					<p class="portfolio_item_view_link">
						<a href="#" target="_blank" class="spiral-button spiral-button_red spiral-button_rounded spiral-button_large ripplelink spiral-button_transparent">View project</a>
					</p>
				</div>
			</div>
		</div>
	</section>



	@endsection
{{-- 
    <!--FOOTER-->
    <?php include('footer.php'); ?>
    <!--///FOOTER-->

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="js/prettify.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/portfolio-init.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	
</body>
</html> --}}