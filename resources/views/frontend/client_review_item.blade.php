@foreach ($reviews as $rev)
    
<li>
    <div class="spiral_posts_shortcode_carousel spiral_posts_shortcode spiral_posts_shortcode-1 clearfix">
        <a class="spiral_latest_news_shortcode_thumb" href="#">
            <img style="height: 400px;" src="{{ asset('public/uploads/review/'.$rev->review_image) }}" alt="review1">
        </a>
        @php
            
        @endphp
        <div class="spiral_latest_news_shortcode_container">
            <div class="date_container">
                
            </div>
            <div class="spiral_latest_news_shortcode_content">
                <h5>
                    <a href="#">{{ $rev->review_title }}</a>
                </h5>
                @php
                    print_r($rev->review_description);
                @endphp
            </div>
        </div>
    </div>
</li>
@endforeach