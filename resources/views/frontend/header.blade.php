
@php
      $services = DB::table('item2')->get();
        $works = DB::table('item1')->get();
       
@endphp
<header id="ABdev_main_header" class="clearfix ">
    <div class="container clearfix">
        <div id="logo">
            <a href="{{ url('/') }}">
                <img id="main_logo" src="{{ asset('public/images/keyper_logo.png') }}" alt="KEYPER">
            </a>
        </div>
        <div class="menu_wrapper">
            <div class="menu_slide_toggle">
                <div class="icon-menu"></div>
            </div>
            <div id="title_breadcrumbs_bar">
                <div class="breadcrumbs">
<!--                    <a href="index.html">Home</a>-->
<!--                    <i class="ci_icon-angle-right"></i>-->
<!--                    <span class="current">Home</span>-->
                </div>
            </div>
            <nav>
                <ul id="navbar_cus">
                    <li id="home"><a href="{{ url('/') }}"><span>Home</span></a></li>

                    <li id="about"><a href="{{ url('about') }}"><span>About</span></a></li>
{{-- 
                    <li id="clients"><a href="about.php"><span>Clients</span></a></li> --}}

                    <li id="work" class="menu-item-has-children">
                        <a href="#" class="scroll"><span>Works</span></a>
                        <ul>
                        @foreach ($works as $service)
                            <li> <a href="{{ url('work/'.$service->id) }}"><span>{{ $service->service_title }}</span></a> </li>
                        @endforeach
                            
                          
                        </ul>
                    </li>

                     <li id="service" class="menu-item-has-children">
                        <a href="#" class="scroll"><span>Service</span></a>
                        <ul>
                        @foreach ($services as $service)
                            <li> <a href="{{ url('service/'.$service->id) }}"><span>{{ $service->service_title }}</span></a> </li>
                        @endforeach
                            
                          
                        </ul>
                    </li>
                   

                    <li id="contact"><a href="{{ url('contact') }}"><span>Contact</span></a></li>

                </ul>
            </nav>
        </div>
    </div>
</header>

<script type="text/javascript" src="{{ asset('public/js/frontend/jquery.js') }}"></script>
<script>
    jQuery( document ).ready(function() {
        console.log(window.location.href);

        var ct = localStorage.getItem("currentTab");
        console.log(ct);
        jQuery('#'+ct).addClass('current-menu-item');

        jQuery('#navbar_cus li').click(function(e){
            localStorage.setItem("currentTab", jQuery(this).attr('id'));
//            alert('clicked');
        });
    });
</script>