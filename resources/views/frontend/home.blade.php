@extends('layouts.frontend')

@section('content')


    <!--SLIDER-->
    @include('frontend.slider')
    <!--///SLIDER-->

    <!--DESCRIPTION SECTION-->
    @include('frontend.description_section')
    <!--///DESCRIPTION SECTION-->


    <!--RELATED SERVICES-->
    @include('frontend.related_services')
    <!--///RELATED SERVICES-->

    <!-- VIDEO SECTION-->
    @include('frontend.video_section')
    <!-- ///VIDEO SECTION-->

    <!-- CLIENT REVIEW-->
    @include('frontend.client_review')
    <!-- ///CLIENT REVIEW-->

<!--    <section class="spiral_section_tc spiral-centered no_padding_top section_bg">-->
<!--        <div class="spiral_section_content">-->
<!--            <div class="spiral_container">-->
<!--                <div class="spiral_column_tc_span12">-->
<!--                    <a href="#" target="_self" class="spiral-button spiral-button_light spiral-button_rounded spiral-button_medium ripplelink spiral-button_transparent ">Visit Blog</a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->

    <!-- CONTACT FORM-->
    @include('frontend.contact_form')
    <!-- ///CONTACT FORM-->


    <!-- MAP-->
    @include('frontend.map')
    <!-- ///MAP-->

@endsection