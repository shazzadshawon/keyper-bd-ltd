<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home | KeyPer</title>
    <meta name="description" content="KeyPer">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('public/images/favicon.png')}}">
    <!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C400italic%2C300%2C700%7CRaleway%3A400%2C200%2C600%2C700&amp;ver=4.2.6" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('public/css/frontend/rs-plugin/css/settings.css') }}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('public/css/frontend/icons/icons.css') }}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('public/css/frontend/core-icons/core_style.css') }}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('public/css/frontend/scripts.css') }}" type="text/css" media="all">
<!--    <link rel="stylesheet" href="css/pace-theme-center-simple.css" type="text/css" media="all">-->
    <link rel="stylesheet" href="{{ asset('public/css/frontend/pace-theme-loading-bar.css') }}" type="text/css" media="all">

    <link rel="stylesheet" href="{{ asset('public/css/frontend/style.css') }}" type="text/css" media="all">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>

<body class="page">
    <!--HEADER-->
    @include('frontend.header')
    <!--///HEADER-->

    @yield('content')

    <!--FOOTER-->
    @include('frontend.footer')
    <!--///FOOTER-->

    <script type="text/javascript" src="{{ asset('public/js/frontend/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRyD_MdHzFbCRYJYi6VMRueG2tqlX_3TQ"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/rs-plugin/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/rs-plugin/js/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/rs-plugin/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/rs-plugin/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/prettify.js') }}"></script>
<!--    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->
<!--    Loader js-->
    <script type="text/javascript" data-pace-options='{ "ajax": false }' src="{{ asset('public/js/frontend/pace.min.js') }}"></script>
<!--    ///Loader js-->
    <script type="text/javascript" src="{{ asset('public/js/frontend/portfolio-init.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/scripts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/frontend/custom.js') }}"></script>

    <script>
        jQuery( document ).ready(function() {
            localStorage.setItem("currentTab", 'home');
        });
    </script>

</body>
</html>

