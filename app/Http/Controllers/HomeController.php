<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    
    public function home(Request $request)
    {

$disk = "/home/rpyytxsz";
//echo(floor(100 * disk_free_space($disk) / disk_total_space($disk)));
//return floor(disk_total_space($disk));

//return floor(100 * disk_free_space($disk) / disk_total_space($disk));






$Bytes = disk_free_space("/home/rpyytxsz/");


$Type=array("", "kilo", "mega", "giga", "tera");
$counter=0;
while($Bytes>=1024)
{
$Bytes/=1024;
$counter++;
}
//return ("".$Bytes." ".$Type[$counter]."bytes");





        $sliders = DB::table('sliders')->get();
        $services = DB::table('item2')->get();
        $works = DB::table('item1')->get();
        $reviews = DB::table('reviews')->get();
        $video = DB::table('video')->first();
       // print_r( $video);
       // exit;
      
      
       
 // echo "<pre>";
 //         print_r($reviews);
 //         exit;
//return $reviews;
        return view('frontend.home',compact('sliders','services','works','reviews','video'));
    }














    public function form()
    {
        $filename ='';
        return view('form',compact('filename'));
    }
    public function upload()
    {
        // $img = Image::make('uploads/foo.jpg');
        // // get file size
        // $size = $img->filesize();
        //return time();
        $filename = time();
        // return (Input::get('name'));
        // return $ext = pathinfo(Input::file('name'), PATHINFO_EXTENSION);

        Image::make(Input::file('name'))->resize(300, 150)->opacity(50)->save('uploads/'.$filename.'.jpg');
        return view('form',compact('filename'));
        //return Input::get('name');
    }

     public function about()
    {
        $abouts = DB::table('abouts')->get();
        //$reviews = array();
        return view('frontend.about',compact('abouts'));
    }     
    public function service($id)
    {
         $service = DB::table('item2')
        ->where('id',$id)
        ->first();

        $images = DB::table('item_images_2')
        ->where('item_id',$id)
        ->get();

        return view('frontend.service',compact('service','images'));
    }    
   public function work($id)
    {
        
         $service = DB::table('item1')
        ->where('id',$id)
        ->first();

        $images = DB::table('item_images_1')
        ->where('item_id',$id)
        ->get();
        return view('frontend.work',compact('service','images')); 
    }    


    public function gallery()
    {
         $galleries = DB::table('galleries')->get();
         //return count($galleries);
        return view('frontend.gallery',compact('galleries'));
    }   

















    public function service_cat($id)
    {
        $services = DB::table('services')
        ->where('service_type_id',$id)
        ->get();

        $type = DB::table('servicetypes')
            ->where('id',$id)
            ->first();
        return view('frontend.service_cat',compact('services','type'));
    }    
    
    public function allpackage()
    {
        $packages = DB::table('packages')->get();
        return view('frontend.allpackage',compact('packages'));
    }  

    

    public function wedding_event($id)
    {
       
        $servic = DB::table('services')->where('id',$id)->get();
        $service = $servic[0];
        return view('frontend.singlewedding',compact('service'));
    }    

    public function birthday_cat()
    {
        $birthdays = DB::table('birthday')->get();
        return view('frontend.allbirthday',compact('birthdays'));
    }  
  public function birthday($id)
    {
       
        $servic = DB::table('birthday')->where('id',$id)->get();
        $service = $servic[0];
        return view('frontend.singlebirthday',compact('service'));
    }    

     public function music_cat()
    {
        $musics = DB::table('music')->get();
        return view('frontend.allmusic',compact('musics'));
    }  
  public function music($id)
    {
       
        $servic = DB::table('music')->where('id',$id)->get();
        $service = $servic[0];
        return view('frontend.singlemusic',compact('service'));
    }    


  public function int($id)
    {
       
        $servic = DB::table('interior')->where('id',$id)->get();
        $service = $servic[0];
        return view('frontend.singleinterior',compact('service'));
    }    
    public function allint($id)
    {
       
        $ct = DB::table('int_categories')->where('id',$id)->get();
        $catid = $ct[0]->id;
        $catname = $ct[0]->cat_name;
        $ints = DB::table('interior')->where('service_sub_cat_id',$catid)->get();
        
        return view('frontend.allint',compact('ints','catname'));
    }    
     public function singlepackage($id)
    {
        $package = DB::table('packages')->where('id',$id)->first();
        return view('frontend.singlepackage',compact('package'));
    }     
  
    public function client_contact()
    {
        return view('frontend.contact_client');
    }    
     public function contact()
    {
        return view('frontend.contact');
    }    
    public function storecontact_client()
    {
        $filename = time().".jpg";

        Image::make(Input::file('contact_image'))->save('public/uploads/contact/'.$filename);

        DB::table('contacts')
        ->insert(
            [
                'contact_title' => Input::get('contact_title'),
                'contact_email' => Input::get('contact_phone'),
                'contact_phone' => Input::get('contact_phone'),
                'contact_image' => $filename,
                'contact_description' => Input::get('contact_description'),
            ]
            );
        return redirect()->back()->with('success','Message Sent Successfully');
    }    

     public function storecontact()
    {
        DB::table('contacts')
        ->insert(
            [
                'contact_title' => Input::get('contact_title'),
                'contact_email' => Input::get('contact_email'),
                'contact_phone' => 0,
                'contact_description' => Input::get('contact_description'),
            ]
            );
        return redirect()->back()->with('success','Message Sent Successfully');
    }    
    


}
