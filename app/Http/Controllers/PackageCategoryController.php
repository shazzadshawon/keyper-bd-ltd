<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class PackageCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = DB::table('int_categories')
         
         ->get();
        return view('backend.packagecategory.int_categories',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        //$types = DB::table('servicetypes')->get();
        return view('backend.packagecategory.addintcategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return 'hy';
         DB::table('int_categories')->insert(
        [
            'cat_name' => Input::get('cat_name'),
  
            'cat_status' => 1,
        ]
        );
         return redirect('packagecategories')->with('success', 'New Category Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $cat = DB::table('int_categories')
        // ->leftjoin('servicetypes', 'categories.cat_type','=','servicetypes.id')
        // ->select('categories.*','servicetypes.type_name')
        ->where('id',$id)
        ->get();
        //return $cat;
        return view('backend.packagecategory.editintcategory',compact('cat'));
    }


    public function update(Request $request, $id)
    {
        //return Input::all();

        DB::table('int_categories')
            ->where('id', $id)
            ->update([
                     'cat_name' => Input::get('cat_name'),
                ]);

            return redirect('packagecategories')->with('success', ' Category Updated Successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $x = DB::table('categories')->where('id', '=', $id)->get();
        // $x->delete();

        DB::table('int_categories')->where('id', $id)->delete();
        //DB::table('categories')->where('id', $id)->delete();

        return redirect('packagecategories')->with('success', ' Category Deleted Successfully');
    }
}
