<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class Item2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'gt';
        $items = DB::table('item2')
                    ->get();
        return view('backend.item2.item2',compact('items'));
    }


    public function add()
    {
        //$int_categories = DB::table('int_categories')->get();
        return view('backend.item2.additem2');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = DB::table('item2')->insertGetId(
        [
            'service_title' => Input::get('service_title'),
            // 'service_sub_cat_id' => Input::get('cat_id'),
            'service_image' => '',
            'service_description' => Input::get('editor1'),
            'service_status' => 1,
        ]
        );

        if (Input::file('images')) {
            $files = $request->file('images');
            foreach ($files as $file) {
                 $filename = rand(1,9999999999999).'.jpg';
                 Image::make($file)->save('public/uploads/item2/'.$filename);

                  DB::table('item_images_2')->insert([
                    'item_id'  => $id,
                    'image_name' => $filename,
                    ]);
            }
        }

       
         return redirect('item2')->with('success', 'New item Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = DB::table('item2')
                ->where('id',$id)->first();
         $item_images = DB::table('item_images_2')
                ->where('item_id',$id)->get();
         //        foreach ($item_images as $key) {
         //            print_r($key);
         //        }

        return view('backend.item2.viewitem2',compact('item','item_images'));
    }



    public function edit($id)
    {
        $services = DB::table('item2')
                    ->where('id',$id)
                    ->get();
        //$cats = DB::table('int_categories')->get();
        $service = $services[0];
        //return $services;
        return view('backend.item2.edititem2',compact('service'));
    }



    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('item2')
            ->where('id', $id)
            ->update([
            'service_title' => Input::get('service_title'),
            'service_description' => Input::get('editor1'),
            'service_status' => 1,
            // 'service_sub_cat_id' => '',
                ]);



            return redirect('item2')->with('success', ' Item Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //   Delete Image
        $item = DB::table('item2')
            ->where('id', $id)->first();
        $item_images = DB::table('item_images_2')->where('item_id', $id)->get();

        DB::table('item2')
            ->where('id', $id)->delete();
         foreach($item_images as $image) {
            if(!empty($image))
            {
                unlink('public/uploads/item2/'.$image->image_name);
            }
        }
        DB::table('item_images_2')->where('item_id', $id)->delete();

         return redirect()->back()->with('success', 'Selected  Item removed Successfully');
    }

  

    public function storeitem2image(Request $request, $id)
    {
        if (Input::file('images')) {
            $files = $request->file('images');
            foreach ($files as $file) {
                 $filename = rand(1,9999999999999).'.jpg';
                 Image::make($file)->save('public/uploads/item2/'.$filename);

                  DB::table('item_images_2')->insert([
                    'item_id'  => $id,
                    'image_name' => $filename,
                    ]);
            }
        }
        return redirect()->back()->with('success', 'Image Added Successfully');
    }


    public function deleteitem2image($id)
    {
        //   Delete Image
        $item = DB::table('item_images_2')->where('id', $id)->first();

       
        $image = $item->image_name;
        if (!empty($image)) {
            unlink('public/uploads/item2/'.$image);
        }
        

         DB::table('item_images_2')->where('id', $id)->delete();

        return redirect()->back()->with('success', 'Selected  Image removed Successfully');
    }

}
