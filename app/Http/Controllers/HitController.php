<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
use Session;


class HitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function check_session_status(Request $request)
    {
        if(empty(session('key')))
        {
            $request->session()->put('key', 'demo');
            $counter = DB::table('hit_counter')->first();
            $hit = $counter->counter;

            $newCount = DB::table('hit_counter')->update(
                [
                    'counter' => $hit+1,
                ]);
            return 'New Hit';
        }
        return session('key');
    }

   public function destroy_session_status()
    {
        $request->session()->forget('key');
        return 'session destroyed';
    }

   

    public function add()
    {
        return view('backend.about.addabout');
    }


    public function store(Request $request)
    {
        //return 'hy';
        DB::table('abouts')->insert(
        [
            'about_description' => Input::get('do'),
        ]
        );
         return redirect('abouts')->with('success', 'New data Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $about = DB::table('abouts')
        ->where('id',$id)
        ->first();
        //return $cat;
        return view('backend.about.editabout',compact('about'));
    }


    public function update(Request $request, $id)
    {
        //return Input::all();

        DB::table('abouts')
            ->where('id', $id)
            ->update([
                     'about_description' => Input::get('do'),
                     
                ]);

            return redirect('abouts')->with('success', 'Data Updated Successfully');


    }

  

    public function destroy($id)
    {
       

        DB::table('abouts')->where('id', $id)->delete();
       

        return redirect('abouts')->with('success', 'Data Deleted Successfully');
    }
}
