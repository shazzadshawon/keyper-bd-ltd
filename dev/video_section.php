<!--Video poster image should be placed in data-background_image attribute of the section-->
<section class="spiral_section_tc spiral-parallax section_body_fullwidth section_bg_image" data-background_image="video/poster2.jpg" data-parallax="0.1">
    <div class="spiral_section_content">
        <div class="spiral_container">
            <div class="spiral_column_tc_span12 center_aligned spiral-animo" data-animation="fadeInUp" data-trigger_pt="0" data-duration="500" data-delay="0">
                <div id="spiral-modal-1" class="spiral-modal">
                    <div class="spiral-modal-button" data-button_id="1">
                        <img src="images/play.png" alt="image button">
                    </div>
                    <div class="spiral-modal-content-wrapper" id="spiral-modal_wrapper_1">
                        <div class="spiral-modal-content " id="spiral-modal_content_1">
                            <div class="spiral-videoWrapper-vimeo">
                                <iframe src="video/demo.mp4" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                            <div class="spiral-modal-close"></div>
                        </div>
                    </div>
                </div>
                <span class="clear spacer_40"></span>
                <h3 class="white_text center_aligned big_text">
                    <span><q>Passion for Perfection</q><br> being the core motto for KEYPER Team.</span>
                </h3>
                <span class="clear spacer_7"></span>
            </div>
        </div>
    </div>
</section>