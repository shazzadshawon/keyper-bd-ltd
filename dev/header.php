<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" media="all">

<header id="ABdev_main_header" class="clearfix ">
    <div class="container clearfix">
        <div id="logo">
            <a href="index.html">
                <img id="main_logo" src="images/keyper_logo.png" alt="KEYPER">
            </a>
        </div>
        <div class="menu_wrapper">
            <div class="menu_slide_toggle">
                <div class="icon-menu"></div>
            </div>
            <div id="title_breadcrumbs_bar">
                <div class="breadcrumbs">
<!--                    <a href="index.html">Home</a>-->
<!--                    <i class="ci_icon-angle-right"></i>-->
<!--                    <span class="current">Home</span>-->
                </div>
            </div>
            <nav>
                <ul id="navbar_cus">
                    <li id="home"><a href="index.php"><span>Home</span></a></li>

                    <li id="about"><a href="about.php"><span>About</span></a></li>

                    <li id="clients"><a href="about.php"><span>Clients</span></a></li>

                    <li id="work" class="menu-item-has-children">
                        <a href="#" class="scroll"><span>Works</span></a>
                        <ul>
                            <li> <a href="detail.php"><span>Keyper Work One</span></a> </li>
                            <li> <a href="detail.php"><span>Keyper Work Two</span></a> </li>
                            <li> <a href="detail.php"><span>Keyper Work Three</span></a> </li>
                        </ul>
                    </li>
                    <li id="service" class="menu-item-has-children">
                        <a href="#" class="scroll"><span>Services</span></a>
                        <ul>
                            <li> <a href="detail.php"><span>Creative Solution</span></a> </li>
                            <li> <a href="detail.php"><span>Advertising</span></a> </li>
                            <li> <a href="detail.php"><span>Event</span></a> </li>
                            <li> <a href="detail.php"><span>Printing &amp; Supply</span></a> </li>
                        </ul>
                    </li>

                    <li id="contact"><a href="contact.php"><span>Contact</span></a></li>

                </ul>
            </nav>
        </div>
    </div>
</header>

<script type="text/javascript" src="js/jquery.js"></script>
<script>
    jQuery( document ).ready(function() {
        console.log(window.location.href);

        var ct = localStorage.getItem("currentTab");
        console.log(ct);
        jQuery('#'+ct).addClass('current-menu-item');

        jQuery('#navbar_cus li').click(function(e){
            localStorage.setItem("currentTab", jQuery(this).attr('id'));
//            alert('clicked');
        });
    });
</script>