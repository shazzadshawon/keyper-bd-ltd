<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Contact | KeyPer</title>
	<meta name="description" content="KeyPer">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
	<!--[if lt IE 9]>
	  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C400italic%2C300%2C700%7CRaleway%3A400%2C200%2C600%2C700&amp;ver=4.2.6" type="text/css" media="all">
	<link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/icons/icons.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/core-icons/core_style.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/scripts.css" type="text/css" media="all">
	<link rel="stylesheet" href="css/style.css" type="text/css" media="all">
</head>

<body class="page">
    <!--HEADER-->
    <?php include('header.php'); ?>
    <!--///HEADER-->

	<section id="headline_breadcrumbs_bar" class="with_image">
		<div class="headline_image headline_style_image">
			<div class="container">
				<div class="row">
					<div class="span12 left_aligned headline_title">
						<h2>Contact</h2>
					</div>
					<div id="button_social" class="headline_button">
						<span class="social_button_title">Lets Be Social</span>
						<span class="social_button_contents">
							<a class="spiral_tooltip" data-gravity="s" href="http://www.facebook.com/keyperltd/" target="_blank" title="Follow us on Facebook"><i class="tmf-facebook"></i></a>
						    <a class="spiral_tooltip" data-gravity="s" href="http://www.instagram.com/keyperltd/" target="_blank" title="Follow us on Instagram"><i class="tmf-instagram"></i></a>
						    <a class="spiral_tooltip" data-gravity="s" href="http://bd.linkedin.com/in/keyper-bd-23a21770" target="_blank" title="Follow us on Linkedin"><i class="tmf-linkedin"></i></a>
						</span>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="spiral_section_tc column_title_left section_with_header">
		<header>
			<div class="spiral_container">
				<h3>
					Get in <strong>Touch</strong>
				</h3>
			</div>
		</header>
        <div class="spiral_section_content">
            <div class="spiral_container">
                <div class="spiral_column_tc_span4">
                    <h3 class="gray_text">
                        <span><q>THE KEYPER Team<q><br>blessed with the little <br>experience we have <br>- accumulated <br> in the past years since <br>- commencement in 2011<br>and career success.”</span>
                    </h3>
                    <span class="clear spacer_30"></span>
                    <div class="gray_text">
                        <p>11/15 , Iqbal Road , Mohammadpur<br>1207 Dhaka, Bangladesh</p>
                        <p>
                            +880 1970 0692 16 <br>
                            +880 1934 0000 85 <br>
                        </p>
                        <p>
                            <a href="mailto:">contact@keyperltd.com</a> <br>
                            <a href="mailto:">keyperbd@gmail.com</a>
                        </p>
                    </div>
                </div>
                <div class="spiral_column_tc_span8">
                    <h2 class="dark_gray">
						<span>
							<strong>Say hello!</strong>
						</span>
                    </h2>
                    <span class="clear spacer_10"></span>
                    <div class="spiralcf" id="spiralcf-wrapper" dir="ltr">
                        <form action="#" method="post" class="contact-form">
                            <div class="hidden">
                                <input type="hidden" name="nonce" value="214a162653">
                                <input type="hidden" name="formid" id="formid" value="contact">
                            </div>
                            <div class="row">
                                <div class="span6">
									<span class="spiralcf-form-control-wrap your-name">
										<input type="text" name="name" value="" size="40" class="spiralcf-text" placeholder="Name">
									</span>
                                </div>
                                <div class="span6">
									<span class="spiralcf-form-control-wrap your-email">
										<input type="email" name="email" value="" size="40" class="spiralcf-text spiralcf-email spiralcf-validates-as-email" placeholder="Email">
									</span>
                                </div>
                            </div>
                            <div class="row">
								<span class="spiralcf-form-control-wrap your-message">
									<textarea name="message" cols="40" rows="10" class="spiralcf-textarea" placeholder="Message"></textarea>
								</span>
                                <br>
                                <input type="submit" value="Send Message" class="spiralcf-submit"  id="spiralcf-submit">
                            </div>
                            <div class="spiralcf-response-output spiralcf-display-none"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</section>


<!--    MAP-->
    <?php include('map.php'); ?>
<!--    ///MAP-->

    <!--FOOTER-->
    <?php include('footer.php'); ?>
    <!--///FOOTER-->

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
	<script type="text/javascript" src="js/prettify.js"></script>
	<script type="text/javascript" src="js/portfolio-init.js"></script>
	<script type="text/javascript" src="js/scripts.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	
</body>
</html>