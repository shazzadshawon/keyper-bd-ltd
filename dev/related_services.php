<section id="related" class="spiral_section_tc spiral-inversed_text section_body_fullwidth section_with_header section_bg">
    <header>
        <div class="spiral_container">
            <h3>Related
                <strong>Services</strong>
            </h3>
        </div>
    </header>
    <div class="spiral_section_content">
        <div class="spiral_container">
            <div class="spiral_column_tc_span12 center_aligned spiral-animo" data-animation="bounceInUp" data-trigger_pt="0" data-duration="1000" data-delay="0">
                <div class="spiral_featured_portfolio clearfix">

                    <!-- one thumbline view is linked for example from related_services_thumbline_view page-->
                    <?php include('related_services_thumbline_view.php'); ?>
                    <!--///thumbline view-->

                    <div class="portfolio_item featured_item  illustrations" data-post_id="84" data-name="Keyper Work Two" data-thumbnail='&lt;img src="images/related_services/event2.jpg" alt="portfolio2" /&gt;' data-image_link="images/related_services/event2.jpg" data-link_to="detail.php" data-cat="Corporate Events" data-date="25 July 2017" data-skills="Event Management, Decoration" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="2">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/event2.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Keyper Work Two</p>
                                <p class="portfolio_item_tags">
                                    Keyper Work Two subheading
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio_item featured_item  illustrations" data-post_id="83" data-name="Keyper Work Three" data-thumbnail='&lt;img src="images/related_services/event3.jpg" alt="portfolio3" /&gt;' data-image_link="images/related_services/event3.jpg" data-link_to="detail.php" data-cat="Corporate Events" data-date="25 July 2017" data-skills="Event Management, Decoration" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="3">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/event3.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Keyper Work Three</p>
                                <p class="portfolio_item_tags">
                                    Keyper Work Three subheading
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio_item featured_item  illustrations" data-post_id="82" data-name="Keyper Work Four" data-thumbnail='&lt;img src="images/related_services/event4.jpg" alt="portfolio4" /&gt;' data-image_link="images/related_services/event4.jpg" data-link_to="detail.php" data-cat="Corporate Events" data-date="25 July 2017" data-skills="Event Management, Decoration" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="4">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/event4.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Keyper Work Four</p>
                                <p class="portfolio_item_tags">
                                    Keyper Work Four subheading
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="portfolio_item featured_item  illustrations" data-post_id="81" data-name="Creative Solution Service One" data-thumbnail='&lt;img src="images/related_services/event10.jpg" alt="portfolio5" /&gt;' data-image_link="images/related_services/event10.jpg" data-link_to="detail.php" data-cat="Creative Solution" data-date="25 July 2017" data-skills="Creativity, New ideas" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="5">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/event10.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Service One</p>
                                <p class="portfolio_item_tags">
                                    Creative Solution Service One subheading
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio_item featured_item  illustrations" data-post_id="80" data-name="Advertising Service One" data-thumbnail='&lt;img src="images/related_services/portfolio15.jpg" alt="portfolio6" /&gt;' data-image_link="images/related_services/portfolio15.jpg" data-link_to="detail.php" data-cat="Adversiting" data-date="25 July 2017" data-skills="Creativity, Photoshop, Illustrator, New ideas" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="6">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/portfolio15.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Advertising One</p>
                                <p class="portfolio_item_tags">
                                    Advertising Service One subheading
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio_item featured_item  illustrations" data-post_id="79" data-name="Printing &amp; Supply Service One" data-thumbnail='&lt;img src="images/related_services/portfolio10.jpg" alt="portfolio7" /&gt;' data-image_link="images/related_services/portfolio10.jpg" data-link_to="detail.php" data-cat="Printing &amp; Supply" data-date="25 July 2017" data-skills="Creativity, Photoshop, Illustrator, New ideas" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="7">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/portfolio10.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Printing Job one</p>
                                <p class="portfolio_item_tags">
                                    Printing &amp; Supply Service One subheading
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio_item featured_item  illustrations" data-post_id="78" data-name="Event Service six" data-thumbnail='&lt;img src="images/related_services/event8.jpg" alt="portfolio8" /&gt;' data-image_link="images/related_services/event8.jpg" data-link_to="detail.php" data-cat="Event Management" data-date="25 July 2017" data-skills="Creativity, Decoration, New ideas" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="8">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/event8.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Event Job Six</p>
                                <p class="portfolio_item_tags">
                                    Event Management Service Six subheading
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="portfolio_item featured_item  illustrations" data-post_id="77" data-name="Keyper Work Two" data-thumbnail='&lt;img src="images/related_services/event2.jpg" alt="portfolio2" /&gt;' data-image_link="images/related_services/event2.jpg" data-link_to="detail.php" data-cat="Corporate Events" data-date="25 July 2017" data-skills="Event Management, Decoration" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="9">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/event2.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Keyper Work Two</p>
                                <p class="portfolio_item_tags">
                                    Keyper Work Two subheading
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio_item featured_item  illustrations" data-post_id="76" data-name="Keyper Work Three" data-thumbnail='&lt;img src="images/related_services/event3.jpg" alt="portfolio3" /&gt;' data-image_link="images/related_services/event3.jpg" data-link_to="detail.php" data-cat="Corporate Events" data-date="25 July 2017" data-skills="Event Management, Decoration" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="10">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/event3.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Keyper Work Three</p>
                                <p class="portfolio_item_tags">
                                    Keyper Work Three subheading
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio_item featured_item  illustrations" data-post_id="75" data-name="Keyper Work Four" data-thumbnail='&lt;img src="images/related_services/event4.jpg" alt="portfolio4" /&gt;' data-image_link="images/related_services/event4.jpg" data-link_to="detail.php" data-cat="Corporate Events" data-date="25 July 2017" data-skills="Event Management, Decoration" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="11">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/event4.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Keyper Work Four</p>
                                <p class="portfolio_item_tags">
                                    Keyper Work Four subheading
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="portfolio_item featured_item  illustrations" data-post_id="74" data-name="Event Service six" data-thumbnail='&lt;img src="images/related_services/event8.jpg" alt="portfolio8" /&gt;' data-image_link="images/related_services/event8.jpg" data-link_to="detail.php" data-cat="Event Management" data-date="25 July 2017" data-skills="Creativity, Decoration, New ideas" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="12">
                        <div class="overlayed">
                            <img class="related_services_poster" src="images/related_services/event8.jpg" alt="portfolio8">
                            <div class="overlay">
                                <div class="portfolio_icons_container">
                                    <a class="portfolio_icon" href="detail.php">
                                        <i class="ci_icon-new-window"></i>
                                    </a>
                                </div>
                                <p class="overlay_title">Event Job Six</p>
                                <p class="portfolio_item_tags">
                                    Event Management Service Six subheading
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
<!--                SLIDER DETAILS PAGE-->
                <?php include('related_services_detailsSlide.php'); ?>
<!--                SLIDER DETAILS PAGE-->

                <span class="clear spacer_29"></span>
                <a href="contact.php" target="_self" class="spiral-button spiral-button_light spiral-button_rounded spiral-button_medium ripplelink spiral-button_transparent ">Contact Us</a>
            </div>
        </div>
    </div>
</section>