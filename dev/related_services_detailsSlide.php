<div id="portfolio_content_details">
    <div id="pointer">
    </div>
    <div class="featured_gallery_post_wrapper">
        <div id="close_wrapper">
            <i class="ci_icon-close"></i>
        </div>
        <div class="gallery_post_image">
            <a class="fancybox" data-fancybox-group="portfolio" href="">
            </a>
        </div>
        <div class="gallery_post_content">
            <div class="gallery_post_links">
                <div class="gallery_post_title">
                    <a href=""></a>
                </div>
                <div class="gallery_post_linkto">
                    <a href=""><i class="ci_icon-new-window"></i></a>
                </div>
            </div>
            <div class="gallery_post_date">
            </div>
            <div class="gallery_post_description">
                <p></p>
            </div>
            <div class="gallery_post_info">
                <div class="gallery_post_category">
                    <span>Categories</span>
                    <span class="cat_names"></span>
                </div>
                <div class="gallery_post_client">
                    <span>Client</span>
                    <span class="client_names"></span>
                </div>
                <div class="gallery_post_skill">
                    <span>Skills</span>
                    <span class="skills_names"></span>
                </div>
            </div>
        </div>
    </div>
</div>