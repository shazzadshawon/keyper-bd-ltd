<div class="portfolio_item featured_item  design illustrations" data-post_id="85" data-name="Keyper Work One" data-thumbnail='&lt;img src="images/related_services/event.jpg" alt="portfolio1" /&gt;' data-image_link="images/related_services/event.jpg" data-link_to="detail.php" data-cat="Corporate Events" data-date="25 July 2017" data-skills="Event Management, Decoration" data-client="Company Name this work is for" data-description=" is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." data-number="1">
    <div class="overlayed">
        <img class="related_services_poster" src="images/related_services/event.jpg" alt="portfolio1">
        <div class="overlay">
            <div class="portfolio_icons_container">
                <a class="portfolio_icon" href="detail.php">
                    <i class="ci_icon-new-window"></i>
                </a>
            </div>
            <p class="overlay_title">Keyper Work One</p>
            <p class="portfolio_item_tags">
                Keyper Work One subheading
            </p>
        </div>
    </div>
</div>