<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

//Route::get('/index', 'HomeController@index');
Route::get('/', 'HomeController@home');
Route::get('/about', 'HomeController@about');
Route::get('/wedding_event/{id}', 'HomeController@wedding_event');
Route::get('/allint/{id}', 'HomeController@allint');
Route::get('/int/{id}', 'HomeController@int');
Route::get('/birthday_cat', 'HomeController@birthday_cat');
Route::get('/birthday/{id}', 'HomeController@birthday');
Route::get('/music_cat', 'HomeController@music_cat');
Route::get('/music/{id}', 'HomeController@music');
Route::get('/contact', 'HomeController@contact');
Route::post('/storecontact', 'HomeController@storecontact');
Route::get('/service/{id}', 'HomeController@service');
Route::get('/work/{id}', 'HomeController@work');







Route::get('/service_category/{id}', 'HomeController@service_cat');
Route::get('/allpackage', 'HomeController@allpackage');
Route::get('/package/{id}', 'HomeController@singlepackage');
Route::get('/gallery', 'HomeController@gallery');
Route::get('/client_contact', 'HomeController@client_contact');
Route::get('/og_contact', 'HomeController@og_contact');
Route::post('/storecontact_client', 'HomeController@storecontact_client');

//Route::post('/form','HomeController@upload'); 


//Hit counter Ajax

// Route::get('/check_session_status', 'HitController@check_session_status');
// Route::get('/destroy_session_status', 'HitController@destroy_session_status');

// Backend routes Start

//Route::get(['middleware' => 'auth'], function () {
Route::group(['middleware' => 'auth'], function(){
    
			Route::get('/shobarjonnoweb', 'DashboardController@index');


			// Package category
			// Route::get('/packagecategories', 'PackageCategoryController@index');
			// Route::get('/addpackagecategory', 'PackageCategoryController@add');
			// Route::post('/storepackagecategory', 'PackageCategoryController@store');
			// Route::get('/editpackagecategory/{id}', 'PackageCategoryController@edit');
			// Route::post('/updatepackagecategory/{id}', 'PackageCategoryController@update');
			// Route::get('/deletepackagecategory/{id}', 'PackageCategoryController@destroy');


			// Interior subcategory
			// Route::get('/intsubcategories', 'SubcategoryController@index');
			// Route::get('/addintsubcategory', 'SubcategoryController@add');
			// Route::post('/storeintsubcategory', 'SubcategoryController@store');
			// Route::get('/editintsubcategory/{id}', 'SubcategoryController@edit');
			// Route::post('/updateintsubcategory/{id}', 'SubcategoryController@update');
			// Route::get('/deleteintsubcategory/{id}', 'SubcategoryController@destroy');

			// // ITEM 1 (work)

			Route::get('/item1', 'Item1Controller@index');
			Route::get('/additem1', 'Item1Controller@add');
			Route::post('/storeitem1image/{id}', 'Item1Controller@storeitem1image');
			Route::get('/viewitem1/{id}', 'Item1Controller@show');
			Route::post('/storeitem1', 'Item1Controller@store');
			Route::get('/edititem1/{id}', 'Item1Controller@edit');
			
			Route::post('/updateitem1/{id}', 'Item1Controller@update');
			Route::get('/deleteitem1image/{id}', 'Item1Controller@deleteitem1image');
			Route::get('/deleteitem1/{id}', 'Item1Controller@destroy');

			Route::get('/work_callback', 'Item1Controller@work_callback');
			

	// // ITEM 1 (work)

			Route::get('/item2', 'Item2Controller@index');
			Route::get('/additem2', 'Item2Controller@add');
			Route::post('/storeitem2image/{id}', 'Item2Controller@storeitem2image');
			Route::get('/viewitem2/{id}', 'Item2Controller@show');
			Route::post('/storeitem2', 'Item2Controller@store');
			Route::get('/edititem2/{id}', 'Item2Controller@edit');
			Route::post('/updateitem2/{id}', 'Item2Controller@update');
			Route::get('/deleteitem2image/{id}', 'Item2Controller@deleteitem2image');
			Route::get('/deleteitem2/{id}', 'Item2Controller@destroy');







			// // /birthday 
			Route::get('/storys', 'StoryController@index');
			Route::get('/addstory', 'StoryController@add');
			Route::post('/storestory', 'StoryController@store');
			Route::get('/editstory/{id}', 'StoryController@edit');
			Route::post('/updatestory/{id}', 'StoryController@update');
			Route::get('/deletestory/{id}', 'StoryController@destroy');
//    Video
			Route::get('/editvideo', 'BirthdayController@edit_video');
			Route::post('/updatevideo/', 'BirthdayController@update_video');

			//  /Service 
			Route::get('/allservices', 'ServiceController@index');
			Route::get('/addservice', 'ServiceController@add');
			Route::post('/storeservice', 'ServiceController@store');
			Route::get('/editservice/{id}', 'ServiceController@edit');
			Route::post('/updateservice/{id}', 'ServiceController@update');
			Route::get('/deleteservice/{id}', 'ServiceController@destroy');
			
			//  /Music 
			// Route::get('/musics', 'MusicController@index');
			// Route::get('/addmusic', 'MusicController@add');
			// Route::post('/storemusic', 'MusicController@store');
			// Route::get('/editmusic/{id}', 'MusicController@edit');
			// Route::post('/updatemusic/{id}', 'MusicController@update');
			// Route::get('/deletemusic/{id}', 'MusicController@destroy');


			// Event

			Route::get('/events', 'EventController@index');
			Route::get('/addevent', 'EventController@add');
			Route::post('/storeevent', 'EventController@store');
			Route::get('/editevent/{id}', 'EventController@edit');
			Route::post('/updateevent/{id}', 'EventController@update');
			Route::get('/deleteevent/{id}', 'EventController@destroy');


			// // package category
			Route::get('/categories', 'CategoryController@index');
			Route::get('/addcategory', 'CategoryController@add');
			Route::post('/storecategory', 'CategoryController@store');
			Route::get('/editcategory/{id}', 'CategoryController@edit');
			Route::post('/updatecategory/{id}', 'CategoryController@update');
			Route::get('/deletecategory/{id}', 'CategoryController@destroy');

			// // Slider
			Route::get('/sliders', 'SliderController@index');
			Route::get('/addslider', 'SliderController@add');
			Route::post('/storeslider', 'SliderController@store');
			Route::get('/editslider/{id}', 'SliderController@edit');
			Route::post('/updateslider/{id}', 'SliderController@update');
			Route::get('/deleteslider/{id}', 'SliderController@destroy');


			// // Gallery
			Route::get('/galleries', 'GalleryController@index');
			Route::get('/addgallery', 'GalleryController@add');
			Route::post('/storegallery', 'GalleryController@store');
			Route::get('/editgallery/{id}', 'GalleryController@edit');
			Route::post('/updategallery/{id}', 'GalleryController@update');
			Route::get('/deletegallery/{id}', 'GalleryController@destroy');


			// // Team
			Route::get('/teammembers', 'TeamController@index');
			Route::get('/viewteammember/{id}', 'TeamController@show');
			Route::get('/addteammember', 'TeamController@add');
			Route::post('/storeteammember', 'TeamController@store');
			Route::get('/editteammember/{id}', 'TeamController@edit');
			Route::post('/updateteammember/{id}', 'TeamController@update');
			Route::get('/deleteteammember/{id}', 'TeamController@destroy');


			// // Review
			Route::get('/reviews', 'ReviewController@index');
			// Route::get('/viewreview/{id}', 'ReviewController@show');
			Route::get('/addreview', 'ReviewController@add');
			Route::post('/storereview', 'ReviewController@store');
			Route::get('/editreview/{id}', 'ReviewController@edit');
			Route::post('/updatereview/{id}', 'ReviewController@update');
			Route::get('/deletereview/{id}', 'ReviewController@destroy');



			// // About
			Route::get('/abouts', 'AboutController@index');
			// Route::get('/viewreview/{id}', 'ReviewController@show');
			Route::get('/addabout', 'AboutController@add');
			Route::post('/storeabout', 'AboutController@store');
			Route::get('/editabout/{id}', 'AboutController@edit');
			Route::post('/updateabout/{id}', 'AboutController@update');
			Route::get('/deleteabout/{id}', 'AboutController@destroy');


			// // cont
			Route::get('/messages', 'ContactController@index');

			Route::get('/deletecontact/{id}', 'ContactController@destroy');

			// // offer
			Route::get('/offers', 'OfferController@index');
			Route::get('/addoffer', 'OfferController@add');

			Route::post('/storeoffer', 'OfferController@store');

			Route::get('/editoffer/{id}', 'OfferController@edit');

			Route::post('/updateoffer/{id}', 'OfferController@update');
			Route::get('/deleteoffer/{id}', 'OfferController@destroy');




});

